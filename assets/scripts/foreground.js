jQuery(document).ready(function() {

  // Log errors
  jQuery(document).foundation(function (response) {
    if (window.console) console.log(response.errors);
  });
  
  // The Echo extension puts an item in personal tools that Foreground really should have in the top menu
  // to make this easier, we move it here and loaded earlier to speed up transform
  jQuery("#pt-notifications").prependTo("#echo-notifications-alerts");
  jQuery("#pt-notifications-message").prependTo("#echo-notifications-messages");
  jQuery("#pt-notifications-alert").prependTo("#echo-notifications-alerts");
  jQuery("#pt-notifications-notice").prependTo("#echo-notifications-notice");
  
  // Turn categories into labels - Rififi revised 17.04.21, edited 05.04.21: and hidden cats too!
  jQuery('#catlinks ul li a').addClass('label');
  
  // Rififi revised 17.04.21, edited 05.04.21 : compatibility with Pro version of Font Awesome - will need to update this to comply with Foreground's new version, which adds the icons using CSS rather than JS, in assets/stylesheets/fontawesome.css.css
  // Append font-awesome icons
  jQuery('[id^=ca-nstab] a').prepend('<div class="drop-icon"><i class="fad fa-book fa-fw"></i></div>');
  jQuery('li#ca-talk a').prepend('<div class="drop-icon"><i class="fad fa-comments fa-swap-opacity fa-fw"></i></div>');
  jQuery('li#ca-form_edit a').prepend('<div class="drop-icon"><i class="fad fa-pen fa-fw"></i></div>');
  jQuery('li#ca-formedit a').prepend('<div class="drop-icon"><i class="fad fa-pen fa-fw"></i></div>');
  jQuery('li#ca-edit a').prepend('<div class="drop-icon"><i class="fad fa-code fa-fw"></i></div>');
  jQuery('li#ca-history a').prepend('<div class="drop-icon"><i class="fas fa-history fa-fw"></i></div>');
  jQuery('li#ca-delete a').prepend('<div class="drop-icon"><i class="fad fa-trash-alt fa-fw"></i></div>');
  jQuery('li#ca-move a').prepend('<div class="drop-icon"><i class="far fa-fingerprint fa-fw"></i></div>');
  jQuery('li#ca-protect a').prepend('<div class="drop-icon"><i class="fad fa-shield-alt fa-fw"></i></div>');
  jQuery('li#ca-unprotect a').prepend('<div class="drop-icon"><i class="fas fa-lock-open-alt fa-fw"></i></div>');
  jQuery('li#ca-watch a').prepend('<div class="drop-icon"><i class="far fa-star fa-fw"></i></div>');
  jQuery('li#ca-unwatch a').prepend('<div class="drop-icon"><i class="fas fa-star fa-fw"></i></div>');
  jQuery('li#ca-purge a').prepend('<div class="drop-icon"><i class="fas fa-redo-alt fa-fw"></i></div>');
  jQuery('li#ca-undelete a').prepend('<div class="drop-icon"><i class="fas fa-undo-alt fa-fw"></i></div>');
  jQuery('li#ca-ask_delete_permanently a').prepend('<div class="drop-icon"><i class="fad fa-minus-octagon fa-fw"></i></div>');
  jQuery('li#t-cite a').prepend('<div class="drop-icon"><i class="fad fa-graduation-cap fa-fw"></i></div>');

if ( jQuery( '#ca-addsection' ).length ) {
  jQuery('li#ca-addsection a').html('<div class="drop-icon"><i class="fa fa-plus-square fa-fw"></i></div>' + jQuery('li#ca-addsection a').attr('title').replace(/\[.+/g,""));
}

  jQuery('li#pt-uls a').prepend('<div class="drop-icon"><i class="fal fa-language fa-fw"></i></div>');
  jQuery('li#pt-userpage a').prepend('<div class="drop-icon"><i class="fas fa-user-circle fa-fw"></i></div>');
  jQuery('li#pt-mytalk a').prepend('<div class="drop-icon"><i class="fad fa-comments fa-fw"></i></div>');
  jQuery('li#pt-adminlinks a').prepend('<div class="drop-icon"><i class="fal fa-user-crown fa-fw"></i></div>');
  jQuery('li#pt-darkmode-link a').prepend('<div class="drop-icon"><i class="far fa-moon-stars fa-fw"></i></div>');
  jQuery('li#pt-preferences a').prepend('<div class="drop-icon"><i class="far fa-sliders-h-square fa-fw"></i></div>');
  jQuery('li#pt-watchlist a').prepend('<div class="drop-icon"><i class="fas fa-star fa-fw"></i></div>');
  jQuery('li#pt-mycontris a').prepend('<div class="drop-icon"><i class="fal fa-user-edit fa-fw"></i></div>');
  jQuery('li#pt-logout a').prepend('<div class="drop-icon"><i class="fas fa-sign-out-alt fa-fw"></i></div>');
  jQuery('li#pt-login a').prepend('<div class="drop-icon"><i class="fas fa-sign-in-alt fa-fw"></i></div>');
  jQuery('li#pt-createaccount a').prepend('<div class="drop-icon"><i class="fa fa-lock-alt fa-fw"></i></div>');
  jQuery('li#pt-anonuserpage a').prepend('<div class="drop-icon"><i class="fa fa-user-secret fa-fw"></i></div>');
  jQuery('li#pt-anontalk a').prepend('<div class="drop-icon"><i class="fal fa-comments fa-fw"></i></div>');
  
  jQuery('li#t-smwbrowselink a').prepend('<div class="drop-icon"><i class="far fa-eye fa-fw"></i></div>');
  jQuery('li#t-curationtoolbar a').prepend('<div class="drop-icon"><i class="fas fa-headset fa-fw"></i></div>');
  jQuery('li#t-whatlinkshere a').prepend('<div class="drop-icon"><i class="fal fa-link fa-fw"></i></div>');
  jQuery('li#t-blockip a').prepend('<div class="drop-icon"><i class="fa fa-ban fa-fw"></i></div>');
  jQuery('li#t-recentchangeslinked a').prepend('<div class="drop-icon"><i class="fad fa-chart-network fa-fw"></i></div>');
  jQuery('li#t-contributions a').prepend('<div class="drop-icon"><i class="fal fa-smile fa-fw"></i></div>');
  jQuery('li#t-log a').prepend('<div class="drop-icon"><i class="fal fa-newspaper fa-fw"></i></div>');
  jQuery('li#t-emailuser a').prepend('<div class="drop-icon"><i class="fab fa-telegram-plane fa-fw"></i></div>');
  jQuery('li#t-userrights a').prepend('<div class="drop-icon"><i class="fal fa-traffic-light-go fa-fw"></i></div>');
  jQuery('li#t-upload a').prepend('<div class="drop-icon"><i class="fal fa-file-import fa-fw"></i></div>');
  jQuery('li#t-specialpages a').prepend('<div class="drop-icon"><i class="fal fa-magic fa-fw"></i></div>');
  jQuery('li#t-print a').prepend('<div class="drop-icon"><i class="fal fa-print fa-fw"></i></div>');
  jQuery('li#t-permalink a').prepend('<div class="drop-icon"><i class="fal fa-crosshairs fa-fw"></i></div>');
  jQuery('li#t-info a').prepend('<div class="drop-icon"><i class="fas fa-info-circle fa-fw"></i></div>');
  jQuery('li#t-cargopagevalueslink a').prepend('<div class="drop-icon"><i class="far fa-search fa-fw"></i></div>');
  jQuery('li#feedlinks a').prepend('<div class="drop-icon"><i class="fa fa-rss fa-fw"></i></div>');

  
  jQuery('ul#toolbox-dropdown.dropdown>li#n-recentchanges a').prepend('<div class="drop-icon"><i class="fad fa-tasks fa-fw"></i></div>');
  jQuery('ul#toolbox-dropdown.dropdown>li#n-help a').prepend('<div class="drop-icon"><i class="fas fa-question fa-fw"></i></div>');
});
